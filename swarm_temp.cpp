#include "kilolib.h"
#include <string.h>

int action = -1;
int action_ttl = 0;
uint32_t action_update = 0;
int action_flag = 0;

uint16_t neg_dist = 65535;
uint16_t pos_dist = 65535;

uint8_t neg_hops = 250;
uint8_t pos_hops = 250;
uint8_t curr_direct = 0;

uint16_t neg_dist_temp = 65535;

message_t message;
int message_received = -1;

uint32_t last_neg_update;
int update_neg = 1;
uint32_t last_pos_update;
int update_pos = 1;


void action_handler(){
  if(message_received == -1){
    set_color(RGB(1, 0, 1));
    int dir = rand_hard() % 3;
    spinup_motors();
    if(dir == 0){
      set_motors(kilo_straight_left, kilo_straight_right);
    }
    else if(dir == 1){
      set_motors(0, kilo_turn_right);
    }
    else{
      set_motors(kilo_turn_left, 0);
    }
    delay(300);

    return;
  }
  else if(message_received == 0){
    set_motors(0,0);
    return;
  }
  uint8_t unk_dir = -1;
  if(neg_dist_temp > neg_dist){
    //set_color(RGB(0, 1, 1));
    unk_dir = curr_direct;
  }
  else if (neg_dist_temp < neg_dist){
    curr_direct = !curr_direct;
    unk_dir = curr_direct;
    //set_color(RGB(1, 0, 0));
  }

  spinup_motors();
  if(unk_dir == -1){
    set_motors(kilo_straight_left, kilo_straight_right);
    delay(300);
  }
  else if(unk_dir == 0){
    set_motors(0, kilo_turn_right);
    delay(1000);
  }
  else{
    set_motors(kilo_turn_left, 0);
    delay(1000);
  }

  neg_dist_temp = neg_dist;
  message_received = 0;

}

void setup(){

  last_neg_update = kilo_ticks;
  last_pos_update = kilo_ticks;
  update_neg = 1;
  update_pos = 1;
  message_received = -1;
  neg_hops = 250;
  pos_hops = 250;
  //spinup_motors();
}


void loop(){
    last_neg_update = kilo_ticks;

  /*if(action_flag){
    if(neg_dist > pos_dist){
      set_color(RGB(1, 0, 0));
    }
    else{
      set_color(RGB(0, 0, 1));
    }
    action_flag = 0;
  }
  else{
    if(action == 0){
      set_color(RGB(1, 0, 0));
    }
    else if(action == 1){    last_neg_update = kilo_ticks;

      set_color(RGB(1, 1, 0));
    }
    else if(action == 2){
      set_color(RGB(0, 1, 0));
    }
    else if(action == 3){message_received == -1
      set_color(RGB(0, 1, 1));
    }
    else if(action == 4){
      set_color(RGB(0, 0, 1));
    }
    else{
      set_color(RGB(0, 0, 0));
    }
    action_flag = 1;
  }*/

  action_handler();

  if(update_neg == 0 && last_neg_update + 132 < kilo_ticks){
    neg_dist = 65535;
    neg_dist_temp = neg_dist;
    update_neg = 1;
    neg_hops = -1;
  }
  else if(update_neg == 1 && last_neg_update + 400 < kilo_ticks){
    neg_dist = 65535;
    neg_dist_temp = neg_dist;
    neg_hops = 250;
    message_received = -1;
  }
  if(update_pos == 0 && last_pos_update + 132 < kilo_ticks ){
    pos_dist = 65535;
    neg_dist_temp = neg_dist;
    update_pos = 1;
    pos_hops = 250;
  }
  else if(update_pos == 1 && last_pos_update + 400 < kilo_ticks ){
    pos_dist = 65535;
    neg_dist_temp = neg_dist;
    pos_hops = 250;
    message_received = -1;
  }

  if(action_ttl > 0){
    action_ttl -= (kilo_ticks - action_update)/2;
    if(action_ttl <= 0){
      action_ttl = 0;
      action = -1;
    }
  }

  set_color(RGB(0, 0, 0));
}

void message_rx(message_t *message, distance_measurement_t *distance_measurement){
  int dist_temp = estimate_distance(distance_measurement);
  if(message->data[0] == 1){
    pos_dist = dist_temp;
    last_pos_update = kilo_ticks;
    update_pos = 0;
    pos_hops = 1;
    //message_received = 1;
  }
  else if(message->data[0] == 0){
    set_color(RGB(0, 0, 1));
    neg_dist = dist_temp;
    last_neg_update = kilo_ticks;
    update_neg = 0;
    neg_hops = 1;
    message_received = 1;
  }
  else if(update_neg && message->data[7] < neg_hops){
    set_color(RGB(1, 0, 0));
    neg_hops = message->data[7] + 1;
    last_neg_update = kilo_ticks;
    uint16_t neg_msg;
    memcpy(&neg_msg, &(message->data[3]), sizeof(neg_msg));
    if((long)dist_temp + (long)neg_msg < 65535){
      neg_dist = dist_temp + neg_msg;
    }
    message_received = 1;
  }
  else if(update_pos && message->data[8] < pos_hops){
    pos_hops = message->data[8] + 1;
    last_pos_update = kilo_ticks;
    uint16_t pos_msg;
    memcpy(&pos_msg, &(message->data[5]), sizeof(pos_msg));
    if((long)dist_temp + (long)pos_msg < 65535){
      pos_dist = dist_temp + pos_msg;
    }
    //message_received = 1;
  }

  if(message->data[2] >= action_ttl){
    action = message->data[1];
    action_update = kilo_ticks;
    action_ttl = message->data[2];
  }

  //set_color(RGB(0, 0, 0));

}

message_t *message_tx(){
  message.type = NORMAL;
  message.data[0] = 3;
  message.data[1] = action;
  message.data[2] = action_ttl;
  memcpy(&message.data[3], &neg_dist, sizeof(neg_dist));
  memcpy(&message.data[5], &pos_dist, sizeof(pos_dist));
  message.data[7] = neg_hops;
  message.data[8] = pos_hops;
  message.crc = message_crc(&message);

  return &message;
}

void message_tx_succes(){
    //do nothing|
}

int main(){
    kilo_init();
    kilo_message_tx = message_tx;
    kilo_message_tx_success = message_tx_succes;
    kilo_message_rx = message_rx;
    kilo_start(setup, loop);

    return 0;
}




/*******************************************************************/




#include "kilolib.h"
#include <string.h>

int action = -1;
int action_ttl = 0;
uint32_t action_update = 0;
int action_flag = 0;

uint16_t neg_dist = 65535;
uint16_t pos_dist = 65535;

uint8_t neg_hops = 255;
uint8_t pos_hops = 255;

uint8_t neg_ttl = 0;
uint8_t pos_ttl = 0;

message_t message;

uint32_t neg_update;
uint32_t pos_update;

uint8_t update_ttl(int ttl, uint32_t update){
  if(ttl > 0){
    ttl -= (kilo_ticks - update)/2;
    if(ttl <= 0){
      ttl = 0;
    }
  }

  return ttl;

}

void action_handler(){
  uint16_t avg_dist = neg_dist /2 + pos_dist/2;
  if(avg_dist < 150){
    set_color(RGB(0,1,0));
  }
  else if(avg_dist < 550){
    set_color(RGB(0,0,1));
  }
  else if (avg_dist < 1000){
    set_color(RGB(1,0,0));
  }
  else{
    return;
  }

  delay(avg_dist/2);
  set_color(RGB(0,0,0));
  delay(avg_dist/2);

}

void setup(){


  //spinup_motors();
}


void loop(){
  action_handler();

  action_ttl = update_ttl(action_ttl, action_update);
  if(action_ttl <= 0){
    action = -1;
  }

  pos_ttl = update_ttl(pos_ttl, pos_update);
  if(pos_ttl <= 0){
    pos_hops = 255;
    pos_dist = 65535;
  }

  neg_ttl = update_ttl(neg_ttl, neg_update);
  if(neg_ttl <= 0){
    neg_hops = 255;
    neg_dist = 65535;
  }

}

void message_rx(message_t *message, distance_measurement_t *distance_measurement){
  int dist_temp = estimate_distance(distance_measurement);
  int curr_ttl = message->data[2];

  if (message->data[0] == 0) { //Negative
      neg_ttl = curr_ttl;
      neg_hops = 0;
      neg_dist = dist_temp;
      neg_update = kilo_ticks;
  }
  else if (message->data[0] == 1) { //Positive
      pos_ttl = curr_ttl;
      pos_hops = 0;
      pos_dist = dist_temp;
      pos_update = kilo_ticks;
  }
  else{ //Anyone Else
    if(message->data[7] < neg_hops){
      neg_ttl = curr_ttl;
      neg_hops = message->data[7]+1;
      uint16_t neg_msg;
      memcpy(&neg_msg, &(message->data[3]), sizeof(neg_msg));
      if((long)dist_temp + (long)neg_msg < 65535){
        neg_dist = dist_temp + neg_msg;
      }
      else{
        neg_dist = 65535;
      }
      neg_update = kilo_ticks;
    }
    if(message->data[8] < pos_hops){
      pos_ttl = curr_ttl;
      pos_hops = message->data[8]+1;
      uint16_t pos_msg;
      memcpy(&pos_msg, &(message->data[5]), sizeof(pos_msg));
      if((long)dist_temp + (long)pos_msg < 65535){
        pos_dist = dist_temp + pos_msg;
      }
      else{
        pos_dist = 65535;
      }
      pos_update = kilo_ticks;
    }
  }

  if(curr_ttl >= action_ttl){
    action = message->data[1];
    action_update = kilo_ticks;
    action_ttl = curr_ttl;
  }
}

message_t *message_tx(){
  message.type = NORMAL;
  message.data[0] = 3;
  message.data[1] = action;
  message.data[2] = action_ttl;
  memcpy(&message.data[3], &neg_dist, sizeof(neg_dist));
  memcpy(&message.data[5], &pos_dist, sizeof(pos_dist));
  message.data[7] = neg_hops;
  message.data[8] = pos_hops;
  message.crc = message_crc(&message);

  return &message;
}

void message_tx_succes(){
    //do nothing|
}

int main(){
    kilo_init();
    kilo_message_tx = message_tx;
    kilo_message_tx_success = message_tx_succes;
    kilo_message_rx = message_rx;
    kilo_start(setup, loop);

    return 0;
}

/*******************************************************************/////

#include "kilolib.h"
#include <string.h>

int action = -1;
uint8_t action_ttl = 0;
uint32_t action_update = 0;
int action_flag = 0;

uint32_t neg_dist = 65535;
int neg_dist_count = 0;
uint32_t pos_dist = 65535;
int pos_dist_count = 0;
uint32_t temp_dist = 65535;

uint8_t neg_hops = 250;
uint8_t pos_hops = 250;

message_t message;
uint8_t neg_dist_update = 0;
uint8_t pos_dist_update = 0;

uint8_t neg_ttl = 0;
uint8_t pos_ttl = 0;
uint8_t motion;

uint32_t neg_update;
uint32_t pos_update;
uint32_t update_interval = 400;
uint32_t last_movement;


int max(long a, long b){return a > b ? a : b; }

void blink(int times, uint8_t color){
  int a = 0;
  for(a = 0; a < times; a++){
    set_color(color);
    delay(70);
    set_color(RGB(0,0,0));
    delay(70);
  }
}

uint8_t update_ttl(uint8_t ttl, uint32_t update){
  uint32_t diff = (kilo_ticks - update)/2;


  if(diff > ttl){
    ttl = 0;
  }
  else{
    ttl -= diff;
  }
  if(ttl <= 0){
    ttl = 0;
  }
  return ttl;
}

void action_handler(){
  pos_dist_update = 0;
  pos_dist_count = 0;
  pos_dist = 0;

  if(neg_dist < 50){
    set_color(RGB(0,1,1));
    return;
  }

  if(neg_dist_update == 0){
    //set_color(RGB(0, 1, 1));
    return;
  }
  neg_dist_update = 0;


  uint8_t unk_dir = 2;

  if(temp_dist == neg_dist){
    blink(2, RGB(0,0,1));
    unk_dir = motion;
  }
  else if (temp_dist < neg_dist){
    blink(2, RGB(1,0,0));
    unk_dir = !motion;
  }

  temp_dist = neg_dist;
  neg_dist = 0;
  neg_dist_count = 0;

  spinup_motors();
  if(unk_dir == 2){
    set_motors(kilo_straight_left, kilo_straight_right);
    //set_color(RGB(1, 1, 1));
    delay(400);
  }
  else if(unk_dir == 0){
    set_motors(0, kilo_turn_right);
    //set_color(RGB(0, 0, 1));
    delay(600);
  }
  else{
    set_motors(kilo_turn_left, 0);
    //set_color(RGB(1, 0, 1));
    delay(600);
  }


  set_motors(0, 0);
  //set_color(RGB(0,0,0));
}


void setup(){
  neg_update=kilo_ticks;
  pos_update=kilo_ticks;
  last_movement=kilo_ticks;
  motion = rand_hard() > 128 ? 0 : 1;
}


void loop(){
  if(neg_hops == 1){
    set_color(RGB(1,0,0));
  }
  else if(neg_hops == 2){
    set_color(RGB(1,1,0));
  }
  else if(neg_hops == 3){
    set_color(RGB(0,1,0));
  }
  else if(neg_hops == 4){
    set_color(RGB(0,1,1));
  }
  else if(neg_hops == 5){
    set_color(RGB(0,0,1));
  }
  else if(neg_hops == 6){
    set_color(RGB(1,0,1));
  }
  else{
    set_color(RGB(1,1,1));
  }

  if(neg_hops >= 4 || kilo_ticks - last_movement >= update_interval/(4 * neg_hops)){
    action_handler();
    last_movement = kilo_ticks;
  }

  action_ttl = update_ttl(action_ttl, action_update);
  if(action_ttl == 0){
    action = -1;
  }

  pos_ttl = update_ttl(pos_ttl, pos_update);
  if(kilo_ticks-pos_update > update_interval){
    pos_hops = 250;
    pos_dist = 65535;
    pos_dist_count = 0;
    pos_dist_update = 0;
    pos_ttl = 0;temp_dist = neg_dist;
  }

  neg_ttl = update_ttl(neg_ttl, neg_update);
  if(kilo_ticks-neg_update > update_interval){
    neg_hops = 250;
    neg_dist = 65535;
    temp_dist = 65535;
    neg_dist_count = 0;
    neg_dist_update = 0;
    neg_ttl = 0;
    blink(5, RGB(0,1,1));
    /*set_color(RGB(1, 0, 1));
    delay(1000);
    set_color(RGB(0, 0, 0))*/;
  }

}

void message_rx(message_t *message, distance_measurement_t *distance_measurement){
  uint8_t dist_temp = estimate_distance(distance_measurement);
  uint32_t neg_dist_diff = -1;
  uint32_t pos_dist_diff = -1;
  message_t mess_persist = *message;
  uint8_t curr_ttl = mess_persist.data[2];
  int curr_neg_hops = neg_hops, curr_pos_hops = pos_hops;


  if (mess_persist.data[0] == 0) { //Negative
      //blink(3, RGB(1,0,1));
      neg_hops = 1;
      neg_dist_diff = dist_temp;
      neg_update = kilo_ticks;
      neg_dist_update = 1;
      neg_ttl = 255;
  }
  else if (mess_persist.data[0] == 1) { //Positive
      //blink(3, RGB(0,1,0));

      pos_hops = 1;
      pos_dist_diff = dist_temp;
      pos_update = kilo_ticks;
      pos_dist_update = 1;
      pos_ttl = 255;
  }
  else{ //Anyone Else
    if(neg_hops > mess_persist.data[7] && neg_ttl < curr_ttl){
      //set_color(RGB(0,1,1));
      neg_ttl = curr_ttl;
      neg_hops = mess_persist.data[7]+1;
      if(neg_hops > 10){
        neg_hops = 10;
      }
      uint16_t neg_msg;
      memcpy(&neg_msg, &(mess_persist.data[3]), sizeof(neg_msg));
      neg_dist_diff = dist_temp + neg_msg;
      neg_update = kilo_ticks;
      neg_dist_update = 1;
    }

    if(pos_hops > mess_persist.data[8] && pos_ttl < curr_ttl){
      pos_ttl = curr_ttl;
      pos_hops = mess_persist.data[8]+1;
      if(pos_hops > 10){
        pos_hops = 10;
      }
      uint16_t pos_msg;
      memcpy(&pos_msg, &(mess_persist.data[5]), sizeof(pos_msg));
      pos_dist_diff = dist_temp + pos_msg;
      pos_update = kilo_ticks;
      pos_dist_update = 1;
    }
  }


  if(curr_neg_hops == neg_hops && neg_dist_diff != -1){
    neg_dist = ((neg_dist_count * neg_dist) + neg_dist_diff)/(neg_dist_count+1);
    neg_dist_count++;
  }
  else if(neg_dist_diff != -1){
    neg_dist_count = 1;
    neg_dist = neg_dist_diff;
  }

  if(curr_pos_hops == pos_hops && pos_dist_diff != -1){
    pos_dist = ((pos_dist_count * pos_dist) + pos_dist_diff)/(1 + pos_dist_count);
    pos_dist_count++;
  }
  else if (pos_dist_diff != -1){
    pos_dist_count = 1;
    pos_dist = pos_dist_diff;
  }

  if(curr_ttl > action_ttl){
    action = mess_persist.data[1];
    action_update = kilo_ticks;
    action_ttl = curr_ttl;
  }
}

message_t *message_tx(){
  message.type = NORMAL;
  message.data[0] = 2;
  message.data[1] = action;
  message.data[2] = action_ttl;
  memcpy(&message.data[3], &neg_dist, sizeof(neg_dist));
  memcpy(&message.data[5], &pos_dist, sizeof(pos_dist));
  message.data[7] = neg_hops;
  message.data[8] = pos_hops;
  message.crc = message_crc(&message);

  return &message;
}

void message_tx_succes(){
    //do nothing
}

int main(){
    kilo_init();
    kilo_message_tx = message_tx;
    kilo_message_tx_success = message_tx_succes;
    kilo_message_rx = message_rx;
    kilo_start(setup, loop);

    return 0;
}
