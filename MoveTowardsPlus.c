#include <kilolib.h>
uint32_t new_distance = 0;
uint32_t min_distance = 2000;
uint32_t min_count = 1;
uint8_t rcvd_message = 0;
uint8_t is_spun = 0;

//MOVE TOWARDS PLUS - BY SLOWLY PIVOTING TOWARDS IT
void setup()
{
        //rcvd_message = 0;
}

void loop()
{
    //causes bots to pivot back and forth moving closer
    if(new_distance <= min_distance  && rcvd_message)
    {
        rcvd_message = 0;
        if(is_spun != 2) spinup_motors();
        set_motors(kilo_straight_left, kilo_straight_right);
        set_color(RGB(0,1,0));//turn green if going straight
        min_distance = new_distance;
        min_count = 1;
        is_spun = 2;
        delay(550);// may want to adjust
    }
    else if(new_distance > min_distance && rcvd_message)
    {
        rcvd_message = 0;
        if(!is_spun) spinup_motors();
        set_motors(0,kilo_turn_right);
        set_color(RGB(1,0,0));//turn red if turning right
        min_distance = (min_count * min_distance / (min_count + 1)) + (new_distance + 2*min_count)/(min_count + 1);
        min_count++;
        is_spun = 1;
        delay(800);// may want to adjust
    }
    else
    {
        set_color(RGB(1,1,1));// turn white if no signal
        set_motors(0,0);
        is_spun = 0;
    }


}

void message_rx(message_t *message, distance_measurement_t *distance_measurement)
{

    if(message->data[0] == 1)
    {
        new_distance = estimate_distance(distance_measurement);
        rcvd_message = 1;
    }

}

int main() {
    kilo_init();
    kilo_message_rx = message_rx;
    kilo_start(setup, loop);
    return 0;
}
