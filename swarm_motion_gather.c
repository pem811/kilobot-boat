#include "kilolib.h"

//Movement globals
uint32_t new_distance = 0;
uint32_t last_distance = 0; //formerly "min_distance"
uint32_t last_count = 1;    //formerly "min_count"
uint8_t rcvd_message = 0;
uint8_t is_spun = 0;

//turn globals
uint8_t turn_flag = 0;

// Constants for orbit control.
#define TOO_CLOSE_DISTANCE 50
#define DESIRED_DISTANCE 60

//General globals
uint8_t state = 0; //0: random motion 1: move towards PLUS
                   //2: move from minus 3: move towards minus
                   //4: move from plus 5: left turn
                   //6: right turn 7: left turn Minus
                   //8: right turn minus 9: gather
uint8_t id, follow_id;
int motion_mod; //-1 or 1. Defines whether left or right is default turn
uint32_t last_update; //time last message received kilo-ticks
uint32_t last_movement; //time since last movement in gather logic
message_t message;
uint8_t hops;


//Action State
int action = 255; //0: do nothing 1:straight 2: left 3: right
                //4: back 255: WTH I'm lost
uint8_t action_ttl = 0;
uint32_t action_update = 0;
int action_flag = 0;

#define INTERVAL 600 //~15 seconds I THINK

//blink for debugging
void blink(int times, uint8_t color){
  int a = 0;
  for(a = 0; a < times; a++){
    set_color(color);
    delay(50);
    set_color(RGB(0,0,0));
    delay(50);
  }
}

uint8_t update_ttl(uint8_t ttl, uint32_t update){
  uint32_t diff = (kilo_ticks - update)/20.0;
  if(diff > ttl){
    ttl = 0;
  }
  else{
    ttl -= diff;
  }
  return ttl;
}

void setup(){
  hops = 255;
  id = 0; //individual id
  follow_id = 255; //id of bot following
  while(id < 2 || id == 255){
    id = rand_hard();
  }
  rand_seed(rand_hard()); //seeds soft random function
  // if ? then : else
  motion_mod = rand_hard() > 128 ? -1 : 1; //defines default motion mod
  last_update = kilo_ticks;
}

//0 straight [1 dominant 2 other] direction
void motion(int type){
  type *= motion_mod; //makes type > or < 0
  if(type == 0){
    set_motors(kilo_straight_left, kilo_straight_right);
  }
  else if(type == -1 || type == 2){
    set_motors(0, kilo_turn_right);
  }
  else{
    set_motors(kilo_turn_left, 0);
  }
}


void move_towards()
{
    //causes bots to pivot back and forth moving closer
    if(new_distance <= last_distance)
    {
        rcvd_message = 0;
        if(is_spun != 2) spinup_motors();
        motion(0);
        set_color(RGB(0,1,0));//turn green if going straight
        last_distance = new_distance;
        last_count = 1;
        is_spun = 2;
        delay(550);// may want to adjust
    }
    else if(new_distance > last_distance)
    {
        rcvd_message = 0;
        if(!is_spun) spinup_motors();
        motion(2);
        set_color(RGB(1,0,0));//turn red if turning
        last_distance = (last_count * last_distance / (last_count + 1)) + (new_distance + 2*last_count)/(last_count + 1);
        last_count++;
        is_spun = 1;
        delay(600);// may want to adjust
    }


}


void move_away(){
    if(new_distance > last_distance)
    {
        if(is_spun != 3) spinup_motors();
        is_spun = 3;
        motion(0);
        set_color(RGB(0,1,0));//turn green if moving straight away from minus source
        last_distance = new_distance;
        last_count = 1;
    }
    else if(new_distance <= last_distance )
    {
        if(!is_spun) spinup_motors();
        is_spun = 1;
        motion(1);
        set_color(RGB(1,0,0));//turn red if turning right to move away from minus
        last_distance = (last_count * last_distance / (last_count + 1)) + new_distance / (last_count + 1);
        last_count++;
    }
    delay(500);// may want to adjust
    rcvd_message = 0;
}

void turn(){
  // Update the motion whenever a message is received.
  if( turn_flag == 0 && new_distance > DESIRED_DISTANCE ){
    move_towards();
    return;
  }
  else if(turn_flag == 0){
    turn_flag = 1;
  }

  // If too close, move forward to get back into orbit.
  /*if (new_distance < TOO_CLOSE_DISTANCE){
    set_color(RGB(1, 1, 0));
    if(state == 5 || state == 7){  //left turn
      if(is_spun < 2) spinup_motors();
      is_spun = 2;
      set_motors(kilo_turn_right, 0);
    }
    else{
      if(is_spun == 0 || is_spun == 2) spinup_motors(); //right turn
      is_spun = 1;
      set_motors(0, kilo_turn_left);
    }
  }
  // If not too close, turn left or right depending on distance,
  // to maintain orbit.
  else */
  if (new_distance < DESIRED_DISTANCE){
    set_color(RGB(1, 0, 1));
    if(state == 5 || state == 7){  //left turn
      if(is_spun == 0 || is_spun == 2) spinup_motors();
      is_spun = 1;
      set_motors(kilo_turn_left, 0);
    }
    else{ //right turn
      if(is_spun < 2) spinup_motors();
      is_spun = 2;
      set_motors(0, kilo_turn_right);
    }
  }
  else if (new_distance < DESIRED_DISTANCE + 20){
    set_color(RGB(0, 1, 1));
    if(state == 5 || state == 7){  //left turn
      if(is_spun < 2) spinup_motors();
      is_spun = 2;
      set_motors(0, kilo_turn_right);
    }
    else{
      if(is_spun == 0 || is_spun == 2) spinup_motors(); //right turn
      is_spun = 1;
      set_motors(kilo_turn_left, 0);
    }
  }
  else{
    turn_flag = 0;
  }
  rcvd_message = 0;
}

//random motion
void random(){
  uint8_t dir = rand_soft()/86; //255/3 + 1
  spinup_motors();
  motion(dir);
  delay(6 * rand_soft());
  set_motors(0,0);
}

void loop(){
  //random!
  if(state == 0){
    blink(3, RGB(1,1,1));
    random();
  }
  //smart states
  else if(rcvd_message){
    if(action == 0){
      state = 0;
      turn_flag = 0;
      return;
    }
    else if(state == 1 /*|| state == 3*/){
      turn_flag = 0;
      blink(3, RGB(0,0,1));
      move_towards();
    }
    else if(state == 2 /*|| state == 4*/){
      turn_flag = 0;
      blink(3, RGB(1,0,0));
      move_away();
    }
    else if(state == 5){
      blink(3, RGB(0,1,1));
      turn();
    }
    else if(state == 6){
      blink(3, RGB(1,0,1));
      turn();
    }
    else if(state == 7){
      blink(3, RGB(1,1,0));
      turn();
    }
    else if(state == 8){
      blink(3, RGB(0,1,0));
      turn();
    }
    else if(state == 9){
      blink(2, RGB(0,0,1));
      if(hops >= 4 || kilo_ticks - last_movement >= INTERVAL/(4 * hops)){
        move_towards();
        last_movement = kilo_ticks;
      }
    }
  }
  else{
    if(!turn_flag && (state >= 5 && state <= 8)){
      set_motors(0,0);
      is_spun = 0;
    }
  }

  // if no update in INTERVAL, we're probs lost
  if(kilo_ticks - last_update > INTERVAL){
    //blink(10, RGB(1,0,1));
    if(state == 9 && follow_id != 255 && hops < 5){
      hops++;
      return;
    }
    hops = 255;
    state = 0;
    last_distance = 0;
    last_count = 1;
    motion_mod = rand_soft();
  }


}

void message_rx(message_t *message, distance_measurement_t *distance_measurement){
    uint16_t curr_dist = estimate_distance(distance_measurement);

    //We've found newer instructions and are avoiding the clueless
    //disasters to society
    if(message->data[2] > action_ttl && message->data[1] != 255){
      if(action != message->data[1]){
        action = message->data[1];
        state = 0;
        motion_mod = rand_soft();
      }
      action_update = kilo_ticks;
      action_ttl = message->data[2];
    }

    //ID Conflict
    if(message->data[0] == id){
      id = 0;
      while(id < 2 || id == message->data[0] || id == 255){
        id = rand_soft();
      }
      return;
    }

    //Calculating distance stuff
    if(message->data[0] == 0 && state != 0 && state != 5 && state != 6){
        if(state == 1 && curr_dist + 20 < last_distance){
          //were moving towards plus but got sidetracked
          state = 2;
          last_distance = curr_dist;
          last_count = 1;
        }
        else if(state == 1) return;
        hops = 1;
        new_distance = curr_dist;
        rcvd_message = 1;
        last_update = kilo_ticks;
    }
    else if(message->data[0] == 1 && state != 0 && state != 7 && state != 8){
        //were moveing from minus, now move towards plus
        if(state == 2 && curr_dist + 20 < last_distance && action == 1){
          state = 1;
          last_distance = curr_dist;
          last_count = 1;
        }
        else if (state == 2) return;

        hops = 1;
        new_distance = curr_dist;
        rcvd_message = 1;
        last_update = kilo_ticks;
    }
    else if(message->data[0] <= 1 && state == 0){ //random now
      if(action == 1){ //move forward
        if(message->data[0] == 1){ //closer to postivite
          state = 1; //go towards it!
          last_distance = curr_dist; //start towards plus
          last_count = 1;
        }
        else if(message->data[0] == 0){ //closer to negative
          //but out of the negative well
          if(curr_dist >= 95) return;
          state = 2; //start away from minus
          last_distance = curr_dist;
          last_count = 1;
        }
        message_rx(message, distance_measurement);
      }
      else if(action == 2 || action == 3){ //left turn || right turn
        if(message->data[0] == 1){ //msg from postivite
          motion_mod = action == 2 ? -1 : 1;
          state = 3 + action; //go towards it! 3+2 = 5, 3+3 = 6 (much hack)
        }
        else if(message->data[0] == 0){ //msg from postivite
          motion_mod = action == 2 ? -1 : 1;
          state = 5 + action; //go towards it! 5+2 = 7, 5+3 = 8 (much hack)
        }
        last_distance = 2000; //start towards plus
        last_count = 1;
        new_distance = curr_dist;
        turn_flag = 0;
        last_update = kilo_ticks;
        rcvd_message = 1;
        hops = 1;
      }
    }
    else if(state == 0 && message->data[7] != 255){
      state = 9;
      follow_id = message->data[0];
      last_distance = 2000;
      new_distance = curr_dist;
      last_update = kilo_ticks;
      last_movement = kilo_ticks;
      rcvd_message = 1;
    }
    else if (state == 9){
      if((message->data[0] == follow_id || follow_id == 255) && message->data[7] <= hops-1 ){
        follow_id = message->data[0];
        last_distance = 2000;
        new_distance = curr_dist;
        hops = message->data[7] + 1;
        last_update = kilo_ticks;
        last_movement = kilo_ticks;
        rcvd_message = 1;
      }
      else if(message->data[0] == follow_id){
        follow_id = 255;
      }
    }
}

message_t *message_tx(){
  //update action states
  action_ttl = update_ttl(action_ttl, action_update);
  if(action_ttl == 0){
    action_update = kilo_ticks;
    action = 255; //LOST HELP
  }

  message.type = NORMAL;
  message.data[0] = id;
  message.data[1] = action;
  message.data[2] = action_ttl;
  //memcpy(&message.data[3], &new_distance, sizeof(new_distance));
  message.data[7] = hops > 5 ? 255 : hops;
  message.crc = message_crc(&message);

  return &message;
}

int main() {
    kilo_init();
    kilo_message_rx = message_rx;
    kilo_message_tx = message_tx;
    kilo_start(setup, loop);
    return 0;
}
