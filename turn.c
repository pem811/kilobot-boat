#include "kilolib.h"

//Plus globals
uint32_t new_distance = 0;
uint32_t last_distance = 0; //formerly "min_distance"
uint32_t last_count = 1;    //formerly "min_count"
uint8_t rcvd_message = 0;
uint8_t is_spun = 0;

//Minus globals
//int new_distance = 0;
//uint32_t max_distance = 0;
//uint32_t max_count = 1;
//uint8_t rcvd_message = 0;

//turn globals
uint8_t turn_flag = 0;

#define DIST_LIMIT 100 // ADJUST TO MAX DISTANCE OT MOVE AWAY

// Constants for orbit control.
#define TOO_CLOSE_DISTANCE 45
#define DESIRED_DISTANCE 50

//General globals
uint8_t state = 0; //0: random motion 1: move towards PLUS
                   //2: move from minus 3: move towards minus
                   //4: move from plus 5: left turn
                   //6: right turn 7: gather
uint8_t id, follow_id;
uint8_t motion_mod; //-1 or 1. Defines whether left or right is default turn
uint32_t last_update; //time last message received kilo-ticks
int action = 2; //0: do nothing 1:straight 2: left 3: right
                //4: back

#define INTERVAL 320 //~10 seconds I THINK

//blink for debugging
void blink(int times, uint8_t color){
  int a = 0;
  for(a = 0; a < times; a++){
    set_color(color);
    delay(50);
    set_color(RGB(0,0,0));
    delay(50);
  }
}

void setup(){
  id = 0; //individual id
  follow_id = 255; //id of bot following
  while(id < 2 && id != 255){
    id = rand_hard();
  }
  rand_seed(rand_hard()); //seeds soft random function
  // if ? then : else
  motion_mod = rand_hard() > 128 ? -1 : 1; //defines default motion mod
  last_update = kilo_ticks;
}

//0 straight [1 dominant 2 other] direction
void motion(int type){
  type *= motion_mod; //makes type > or < 0
  if(type == 0){
    set_motors(kilo_straight_left, kilo_straight_right);
  }
  else if(type == -1 || type == 2){
    set_motors(0, kilo_turn_right);
  }
  else{
    set_motors(kilo_turn_left, 0);
  }
}

void move_towards()
{
    //causes bots to pivot back and forth moving closer
    if(new_distance <= last_distance)
    {
        rcvd_message = 0;
        if(is_spun != 2) spinup_motors();
        motion(0);
        set_color(RGB(0,1,1));//turn green if going straight
        last_distance = new_distance;
        last_count = 1;
        is_spun = 2;
        delay(550);// may want to adjust
    }
    else if(new_distance > last_distance)
    {
        rcvd_message = 0;
        if(!is_spun) spinup_motors();
        motion(2);
        set_color(RGB(1,0,1));//turn red if turning
        last_distance = (last_count * last_distance / (last_count + 1)) + (new_distance + 2*last_count)/(last_count + 1);
        last_count++;
        is_spun = 1;
        delay(800);// may want to adjust
    }
}

void turn(){
  // Update the motion whenever a message is received.
  if (rcvd_message == 1){
    rcvd_message = 0;

    if(turn_flag == 0 && new_distance >  DESIRED_DISTANCE - 5){
      move_towards();
      return;
    }
    else if(turn_flag == 0){
      turn_flag = 1;
    }

    // If too close, move forward to get back into orbit.
    if (new_distance < TOO_CLOSE_DISTANCE){
      set_color(RGB(0, 1, 0));
      if(is_spun != 3) spinup_motors();
      is_spun = 3;
      set_motors(kilo_straight_left, kilo_straight_right);
    }
    // If not too close, turn left or right depending on distance,
    // to maintain orbit.
    else  if (new_distance < DESIRED_DISTANCE){
      set_color(RGB(1, 0, 0));
      if(state == 5){  //left turn
        if(is_spun != 1) spinup_motors();
        is_spun = 1;
        set_motors(kilo_turn_left, 0);
      }
      else{ //right turn
        if(is_spun != 2) spinup_motors();
        is_spun = 2;
        set_motors(0, kilo_turn_right);
      }
    }
    else if (new_distance < DESIRED_DISTANCE + 20){
      set_color(RGB(0, 0, 1));
      if(state == 5){  //left turn
        if(is_spun != 2) spinup_motors();
        is_spun = 2;
        set_motors(0, kilo_turn_right);
      }
      else{
        if(is_spun != 1) spinup_motors(); //right turn
        is_spun = 1;
        set_motors(kilo_turn_left, 0);
      }
    }
    else{
      turn_flag = 0;
    }
  }
}

void random(){
  uint8_t dir = rand_soft()/86; //255/3 + 1
  spinup_motors();
  motion(dir);
  delay(800);
  set_motors(0,0);
}

void loop(){
  if(state == 0){
    blink(3, RGB(1,1,1));
    random();
  }
  //smart states
  else if(rcvd_message){
    if(state == 5){
      blink(3, RGB(0,0,1));
      turn();
    }
    else if(state == 6){
      blink(3, RGB(1,0,0));
      turn();
    }
  }
  else{
    //set_motors(0,0);
    //is_spun = 0;
  }

  // if no update in INTERVAL, we're probs lost
  if(kilo_ticks - last_update > INTERVAL){
    blink(5, RGB(1,0,1));
    last_update = kilo_ticks;
    state = 0;
    last_distance = 0;
    last_count = 1;
  }

}

void message_rx(message_t *message, distance_measurement_t *distance_measurement){
    uint16_t curr_dist = estimate_distance(distance_measurement);
    if(message->data[0]==1 && state != 0){
      if(state == 5 || state == 6){
        rcvd_message = 1;
        new_distance = curr_dist;
        last_update = kilo_ticks;
      }
    }

    else if(state == 0){ //random now
      if(action == 2){ //left turn
        if(message->data[0] == 1){ //closer to postivite
          state = 5; //go towards it!
          last_distance = 0; //start towards plus
          last_count = 1;
          new_distance = curr_dist;
          turn_flag = 0;
        }
      }
    }


}

int main(){
    kilo_init();
    kilo_message_rx = message_rx;
    kilo_start(setup, loop);

    return 0;
}
