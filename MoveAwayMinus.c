//move away minus
#include <kilolib.h>
int new_distance = 0;
uint32_t max_distance = 0;
uint32_t max_count = 1;
uint8_t rcvd_message = 0;

#define DIST_LIMIT 100 // ADJUST TO MAX DISTANCE OT MOVE AWAY

// MOVE AWAY MINUS
void setup()
{
        rcvd_message = 0;
}

void loop()
{
    //causes bots pivot then move straight to get away
    if(new_distance > DIST_LIMIT){
        set_color(RGB(0,1,1));// turn cyan if at limit
        set_motors(0,0);
    }
    else if(new_distance >= max_distance && rcvd_message)
    {
        spinup_motors();
        set_motors(kilo_straight_left,kilo_straight_right);
        set_color(RGB(0,1,0));//turn green if moving straight away from minus source
        max_distance = new_distance;
        max_count = 1;
    }
    else if(new_distance < max_distance  && rcvd_message)
    {
        spinup_motors();
        set_motors(0,kilo_turn_right);
        set_color(RGB(1,0,0));//turn red if turning right to move away from minus
        max_distance = (max_count * max_distance / (max_count + 1)) + new_distance / (max_count + 1);
        max_count++;
    }
    else
    {
        set_color(RGB(1,1,1));// turn white if no signal
        set_motors(0,0);
    }
    rcvd_message = 0;
    delay(500);// may wnat to adjust
}

void message_rx(message_t *message, distance_measurement_t *distance_measurement)
{

    if(message->data[0] == 0){
        new_distance = estimate_distance(distance_measurement);
        rcvd_message = 1;
    }


}

int main() {
    kilo_init();
    kilo_message_rx = message_rx;
    kilo_start(setup, loop);
    return 0;
}
