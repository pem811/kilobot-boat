#include "kilolib.h"

//Constants for...everything.
//All tuning could theroretically happen here.

// Constants for light level.

//stop, motion 0 R
#define LEV1 720
//move forward, motion 1 Y
#define LEV2 970
//move left, motion 2 G
#define LEV3 990
//move right, motion 3 C
#define LEV4 1000
//move backward, motion 4 B

#define NUMBER_SAMPLES 200


//Set to determine bot position.
#define ID 1 //0 is 'Negative,' 1 is 'Positive'


int current_light = 0;
int current_action = -1; //set to start some sort of message
message_t message;


//shamelessly copied from the labs
void sample_light()
{

    int number_of_samples = 0;
    long sum = 0;

    while (number_of_samples < NUMBER_SAMPLES)
    {
        int sample = get_ambientlight();

        // -1 indicates a failed sample, which should be discarded.
        if (sample != -1)
        {
            sum = sum + sample;
            number_of_samples = number_of_samples + 1;
        }
    }

    // Compute the average.
    current_light = sum / number_of_samples;
}

void send_action_message(int action){
  if(action != current_action){
    message.type = NORMAL;
    message.data[0] = ID;
    message.data[1] = action;
    message.crc = message_crc(&message);

    current_action = action;
  }
}


void setup()
{
    //EMPTY!
}

void loop()
{
    sample_light();


    if(current_light < LEV1){
      //Do Nothing
      send_action_message(0);
      set_color(RGB(1, 0, 0));
    }
    else if(current_light < LEV2){
      //go straight
      send_action_message(1);
      set_color(RGB(1, 1, 0));
    }
    else if(current_light < LEV3){
      //go left
      send_action_message(2);
      set_color(RGB(0, 1, 0));
    }
    else if(current_light < LEV4){
      //go right
      send_action_message(3);
      set_color(RGB(0, 1, 1));
    }
    else{
      //go backwards
      send_action_message(4);
      set_color(RGB(0, 0, 1));
    }
}

message_t *message_tx()
{
    return &message;
}

void message_tx_success()
{
  //EMPTY!
}

int main()
{
    kilo_init();
    kilo_message_tx = message_tx;
    kilo_message_tx_success = message_tx_success;
    kilo_start(setup, loop);

    return 0;
}
