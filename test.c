#include <stdio.h>
#include <stdint.h>
#include <string.h>

int main(){
  uint16_t bs = 65535;
  uint8_t asdf[2];
  memcpy(asdf, &bs, sizeof(asdf));

  printf("%i %i\n", asdf[0], asdf[1]);

  memcpy(&bs, asdf, sizeof(bs));

  printf("%i\n", bs);

  return 0;

}
