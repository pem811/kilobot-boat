#include "kilolib.h"

// Constants for orbit control.
#define TOO_CLOSE_DISTANCE 40
#define DESIRED_DISTANCE 60

// Constants for motion handling function.
#define STOP 0
#define FORWARD 1
#define LEFT 2
#define RIGHT 3

int current_motion = STOP;
int distance;
int new_message = 0;
int turn_state = 0;
uint32_t last_state_update;

// Function to handle motion.
void set_motion(int new_motion)
{
    // Only take an action if the motion is being changed.
    if (current_motion != new_motion)
    {
        current_motion = new_motion;

        if (current_motion == STOP)
        {
            set_motors(0, 0);
        }
        else if (current_motion == FORWARD)
        {
            spinup_motors();
            set_motors(kilo_straight_left, kilo_straight_right);
        }
        else if (current_motion == LEFT)
        {
            spinup_motors();
            set_motors(kilo_straight_left, 0);
            turn_state = 1;
            last_state_update = kilo_ticks;
        }
        else if (current_motion == RIGHT)
        {
            spinup_motors();
            set_motors(0, kilo_straight_right);
            turn_state = 1;
            last_state_update = kilo_ticks;
        }
    }
    else if(last_state_update + 15 < kilo_ticks){
      if (current_motion == LEFT){
        if(turn_state){
          set_motors(kilo_straight_left, kilo_straight_right);
          set_color(RGB(1, 1, 0));
          turn_state = 0;
          last_state_update = kilo_ticks;
        }
        else{
          set_motors(kilo_straight_left, 0);
          set_color(RGB(1, 0, 0));
          turn_state = 1;
          last_state_update = kilo_ticks;
        }
      }
      else if (current_motion == RIGHT){
        if(turn_state){
          set_motors(kilo_straight_left, kilo_straight_right);
          set_color(RGB(0, 1, 1));
          turn_state = 0;
          last_state_update = kilo_ticks;
        }
        else{
          set_motors(0, kilo_straight_right);
          set_color(RGB(0, 0, 1));
          turn_state = 1;
          last_state_update = kilo_ticks;
        }
      }
    }
}

void setup(){
  last_state_update = kilo_ticks;
}

void loop()
{
    // Update the motion whenever a message is received.
    if (new_message == 1)
    {
        new_message = 0;

        // If too close, move forward to get back into orbit.
        if (distance < TOO_CLOSE_DISTANCE)
        {
            set_color(RGB(1, 0, 0));
            set_motion(LEFT);
        }
        // If not too close, turn left or right depending on distance,
        // to maintain orbit.
        else
        {
            if (distance > DESIRED_DISTANCE)
            {
                set_color(RGB(0, 0, 1));
                set_motion(RIGHT);
            }
            else
            {
                set_color(RGB(0, 1, 0));
                set_motion(FORWARD);
            }
        }
    }
}

void message_rx(message_t *m, distance_measurement_t *d)
{
    new_message = 1;
    distance = estimate_distance(d);
}

int main()
{
    kilo_init();
    kilo_message_rx = message_rx;
    kilo_start(setup, loop);

    return 0;
}
