#include "kilolib.h"
#include <string.h>

#define INTERVAL 320


int action = -1;
uint8_t action_ttl = 0;
uint32_t action_update = 0;
int action_flag = 0;

uint8_t hops;
uint32_t mov_dist, mov_dist_temp;
uint8_t mov_dist_count;
uint8_t id, follow_id;
uint32_t follow_update = 0;
uint8_t interval_updater = 1;

uint8_t motion;
uint32_t last_movement;
uint8_t status;

message_t message;

int max(long a, long b){return a > b ? a : b; }

void blink(int times, uint8_t color){
  int a = 0;
  for(a = 0; a < times; a++){
    set_color(color);
    delay(50);
    set_color(RGB(0,0,0));
    delay(50);
  }
}

uint8_t update_ttl(uint8_t ttl, uint32_t update){
  uint32_t diff = (kilo_ticks - update)/2;
  if(diff > ttl){
    ttl = 0;
  }
  else{
    ttl -= diff;
  }
  return ttl;
}


void action_handler(){
  if(follow_id < 2 && mov_dist < 70){
    return;
  }
  if(mov_dist_count > 0){
    uint32_t loc_dist = mov_dist;
    mov_dist = 0;
    mov_dist_count = 0;

    spinup_motors();
    if(mov_dist_temp > loc_dist){
      set_motors(kilo_straight_left, kilo_straight_right);
      delay(1000);
      blink(2, RGB(0,1,0));
    }
    else if(mov_dist_temp < loc_dist){
      if(motion)
        set_motors(0, kilo_turn_right);
      else
        set_motors(kilo_turn_left, 0);

      //set_color(RGB(0, 0, 1));
      delay(1400);
      blink(2, RGB(1,0,0));
    }
    else{
      if(motion)
        set_motors(kilo_turn_left, 0);
      else
        set_motors(0, kilo_turn_right);
      delay(500);
      blink(2, RGB(0,0,1));
    }
    set_motors(0, 0);
    mov_dist_temp = loc_dist;

  }
}


void setup(){
  hops = 255;
  mov_dist = 255;
  mov_dist_temp = 255;
  mov_dist_count = 0;
  status = 0;
  follow_update = kilo_ticks;
  id = 0;
  follow_id = 255;
  while(id < 2 && id != 255){
    id = rand_hard();
  }
  rand_seed(rand_hard());
  motion = rand_hard() > 128 ? 0 : 1;
}


void loop(){
  if(hops >= 4 || kilo_ticks - last_movement >= INTERVAL/(4 * hops)){
    action_handler();
    last_movement = kilo_ticks;
  }

  if(hops == 1){
    set_color(RGB(1,0,0));
  }
  else if(hops == 2){
    set_color(RGB(1,1,0));
  }
  else if(hops == 3){
    set_color(RGB(0,1,0));
  }
  else if(hops == 4){
    set_color(RGB(0,1,1));
  }
  else if(hops == 5){
    set_color(RGB(0,0,1));
  }
  else if(hops == 6){
    set_color(RGB(1,0,1));
  }
  else{
    set_color(RGB(1,1,1));
  }

  action_ttl = update_ttl(action_ttl, action_update);
  if(action_ttl == 0){
    action = -1;
  }

  if(kilo_ticks - follow_update > INTERVAL/interval_updater){
    if(follow_id != 255 && hops < 250){
      hops++;
    }
    motion = !motion;
    mov_dist = 0;
    mov_dist_temp = 255;
    mov_dist_count = 0;
    follow_id = 255;
    interval_updater++;
    follow_update = kilo_ticks;
    blink(5, RGB(1,0,1));
  }

}

void message_rx(message_t *message, distance_measurement_t *distance_measurement){
  uint8_t dist_temp = estimate_distance(distance_measurement);
  if(follow_id > 1 && (message->data[0] == 0 || message->data[0] == 1)){
    hops = 1;
    mov_dist = dist_temp;//((((uint32_t)mov_dist_count * (uint32_t) mov_dist) + (uint32_t)dist_temp)/((uint32_t) mov_dist_count+1));
    mov_dist_temp = mov_dist;
    mov_dist_count++;
    mov_dist_temp = dist_temp;
    follow_id = message->data[0];
    follow_update = kilo_ticks;
    interval_updater = 1;
    motion = !motion;
    //blink(3, RGB(1,0,0));
  }
  else if( message->data[0] == follow_id && message->data[7] <= hops-1 ){
    uint32_t msg_dist;
    memcpy(&msg_dist, &(message->data[3]), sizeof(msg_dist));
    mov_dist = dist_temp; // + msg_dist;//((((uint32_t)mov_dist_count * (uint32_t) mov_dist) + (uint32_t)dist_temp)/((uint32_t) mov_dist_count+1));
    mov_dist_count++;
    hops = message->data[7] + 1;
    follow_update = kilo_ticks;
    interval_updater = 1;
    //blink(3, RGB(0,1,0));
  }
  else if(follow_id == 255 && message->data[7] <= hops-1){
    uint32_t msg_dist;
    memcpy(&msg_dist, &(message->data[3]), sizeof(msg_dist));
    follow_id = message->data[0];
    mov_dist = dist_temp; // + msg_dist;
    //mov_dist_temp = dist_temp;
    mov_dist_count = 1;
    hops = message->data[7] + 1;
    follow_update = kilo_ticks;
    interval_updater = 1;
    //blink(3, RGB(0,0,1));
  }
  else if(message->data[0] == follow_id){
    follow_id = 255;
  }

  if(message->data[0] == id){
      id = 0;
      while(id < 2 && id != message->data[0] && id != 255){
        id = rand_soft();
      }
      blink(5, RGB(1,1,1));
  }

  if(message->data[2] > action_ttl){
    action = message->data[1];
    action_update = kilo_ticks;
    action_ttl = message->data[2];
  }
}

message_t *message_tx(){
  message.type = NORMAL;
  message.data[0] = id;
  message.data[1] = action;
  message.data[2] = action_ttl;
  memcpy(&message.data[3], &mov_dist, sizeof(mov_dist));
  //memcpy(&message.data[5], &pos_dist, sizeof(pos_dist));
  message.data[7] = hops;


  message.crc = message_crc(&message);

  return &message;
}

void message_tx_succes(){
    //do nothing
}

int main(){
    kilo_init();
    kilo_message_tx = message_tx;
    kilo_message_tx_success = message_tx_succes;
    kilo_message_rx = message_rx;
    kilo_start(setup, loop);

    return 0;
}
